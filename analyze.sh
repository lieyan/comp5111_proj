#!/bin/sh

if [ "$1" = "clean" ]
then
	rm -f *.dot *.ps *.bc *.o *.log
	exit 0
fi

# analyze using llvm built-in functionality
opt -analyze -dot-callgraph $1
# dot -Tps -ocallgraph.ps callgraph.dot

# analyze using canary
canary -inter-aa-eval -dot-may-callgraph $1 
# mv "$1.maycg.dot" "all.maycg.dot"
# dot -Tps -oall.ps "all.maycg.dot"

