#include <memory>

#include "GraphNode.h"

using namespace draw_utility;

NodePtr makeNode(int uid, double weight, std::string label, vec2 position)
{
    assert(weight > 0);
    NodePtr ans = new Node(uid, weight);
    ans->setLabel(label);
    ans->setPosition(position);
    return ans;
}


Node::Node(int uid, double weight)
{
    assert(weight > 0);
    this->uid = uid;
    this->weight = weight;
    this->_visible = false;
}

Node::~Node()
{
    for (auto p : children) {
        delete p;
    }
    printf("GraphNode %d destroyed\n", this->uid);
}

int Node::getUid() const
{
    return uid;
}

int Node::getWeight() const
{
    return weight;
}

void Node::setWeight(int weight)
{
    assert(weight > 0);
    this->weight = weight;
}

std::string Node::getLabel() const
{
    return label;
}
std::vector<NodePtr> Node::getChildren() const
{
    return children;
}

std::vector<NodePtr> Node::getParent() const
{
    return parent;
}
void Node::setLabel(std::string label)
{
    this->label = label;
}

void Node::setPosition(vec2 pos)
{
    this->position[0] = pos[0];
    this->position[1] = pos[1];
 }

void Node::addParent(const std::vector<NodePtr>& rhs)
{
    for (const NodePtr& t : rhs)
        parent.push_back(t);
}

void Node::addChild(const NodePtr& child)
{
    children.push_back(child);
}

void Node::addChildren(const std::vector<NodePtr>& rhs)
{
    for (const NodePtr& t : rhs)
        children.push_back(t);
}


bool Node::node_visible()
{
    return _visible;
}



void Node::Render()
{
    if (!node_visible())
        return;
   //glColor3ub(0, 0, 0);


    drawLabelledNode(position, label);
   
    for (NodePtr p : children) {
      drawArrowedEdge(position, p->position);
       
        p->Render();
    }
    for (NodePtr p : parent) {
      drawArrowedEdge(p->position, position);
        p->Render();
    }
}

void Node::Pick()
{
    if (!node_visible())
        return;

    printf("Node::Pick %d\n", uid);
    
    GLubyte color[3];
    utility::id2color(uid, color);
    glColor3ubv(color);
    drawNode(position);
    for (NodePtr p : children) {
        p->Pick();
    }
   for (NodePtr p : parent) {
        p->Pick();
    }
}

