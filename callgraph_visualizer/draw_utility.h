//  draw_utility.h
//  callgraph
//
//  Created by robin on 3/21/14.
//  Copyright (c) 2014 Lie Yan. All rights reserved.
//

#ifndef __callgraph__draw_utility__
#define __callgraph__draw_utility__

#ifdef __APPLE__
#include "TargetConditionals.h"
#ifdef TARGET_OS_MAC
#include <GLUT/glut.h>
#include <OpenGL/OpenGL.h>
#endif
#elif defined _WIN32 || defined _WIN64
#include <windows.h>
#include <GL/glut.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif

#include <string>
#include "vec2.h"
#include "rect.h"
#include "font_manager.h"

namespace draw_utility
{
    void drawNode(vec2 position);
    void drawLabel(vec2 position, const std::string& label);
    void drawLabelledNode(vec2 position, const std::string& label);
    void drawArrowedEdge(vec2 from, vec2 to);

    void drawPickRectangle(vec2 lt, vec2 br);
    void drawRectangle(vec2 lt, vec2 br);
    void drawSquare(float side);
    void drawText(const char * str);
    void drawSegment(const vec2& a, const vec2& b);
    void drawArrow(const vec2& from, const vec2& to);

    double lineHeight();
    rect BBox(const std::string& str);
};

#endif /* defined(__callgraph__draw_utility__) */
