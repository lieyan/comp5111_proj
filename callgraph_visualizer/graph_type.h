/* 
 * File:   graph_type.h
 * Author: robin
 *
 * Created on April 15, 2014, 4:13 PM
 */

#ifndef GRAPH_TYPE_H
#define	GRAPH_TYPE_H
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/topology.hpp>
#include <boost/random/linear_congruential.hpp>
#include <boost/progress.hpp>
#include <boost/graph/fruchterman_reingold.hpp>
#include <boost/graph/random_layout.hpp>

#include <boost/graph/graphviz.hpp>
#include <boost/graph/iteration_macros.hpp>

#include "vec2.h"

typedef boost::rectangle_topology<> topology_type;
typedef topology_type::point_type point_type;

struct Vertex
{
    std::string id;
    std::string label;
    int uid; // unieque id, also used as pick id
    vec2 position;
    int level;
    //    bool selected;
    bool display = false;
};

struct Edge
{
    std::string label;
    int weight;
};

typedef boost::adjacency_list<boost::setS, boost::vecS,
boost::bidirectionalS, Vertex, Edge,
boost::property<boost::graph_name_t, std::string> > DiGraph;

#endif	/* GRAPH_TYPE_H */

