//
//  vec2.h
//  callgraph
//
//  Created by robin on 3/21/14.
//  Copyright (c) 2014 Lie Yan. All rights reserved.
//

#ifndef __callgraph__vec2__
#define __callgraph__vec2__

#include <iostream>
#include <cassert>
#include <cmath>

class vec2
{
    double data[2];

public:

    vec2()
    {
        vec2(0, 0);
    }

    vec2(double x, double y)
    {
        data[0] = x;
        data[1] = y;
    }

    double const& operator[](int index) const
    {
        assert(index >= 0 && index < 2);
        return data[index];
    }

    double& operator[] (int index)
    {
        assert(index >= 0 && index < 2);
        return data[index];
    }

    vec2 operator-(const vec2 & rhs) const
    {
        return vec2(data[0] - rhs.data[0], data[1] - rhs.data[1]);
    }

    vec2 operator+(const vec2 & rhs) const
    {
        return vec2(data[0] + rhs.data[0], data[1] + rhs.data[1]);
    }

    vec2 operator *(double s) const
    {
        return vec2(data[0] * s, data[1] * s);
    }

    double norm() const
    {
        return sqrt(data[0] * data[0] + data[1] * data[1]);
    }

    vec2 unit_vector() const
    {
        assert(norm() != 0);
        return vec2(data[0] / norm(), data[1] / norm());
    }

    // rotate 90 degree counter-clockwise

    vec2 perp_vector() const
    {
        return vec2(-data[1] / norm(), data[0] / norm());
    }
};



#endif /* defined(__callgraph__vec2__) */
