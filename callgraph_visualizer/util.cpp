

#include "util.h"

#include <string>
#include <cassert>

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/topology.hpp>
#include <boost/random/linear_congruential.hpp>
#include <boost/progress.hpp>
#include <boost/graph/fruchterman_reingold.hpp>
#include <boost/graph/random_layout.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/iteration_macros.hpp>

namespace utility {

    void loadDiGraph(const std::string& path,
            DiGraph& g,
            boost::dynamic_properties& dp,
            std::unordered_map<int, DiGraph::vertex_descriptor>& uid2vertex) {
        // fields of Vertex
        dp.property("node_id", get(&Vertex::id, g));
        dp.property("label", get(&Vertex::label, g));
        dp.property("uid", get(&Vertex::uid, g));
        // fields of Edge
        dp.property("label", get(&Edge::label, g));
        dp.property("weight", get(&Edge::weight, g));

        // read from dot file
        std::ifstream dotfile(path);
        read_graphviz(dotfile, g, dp);

        // initialize other fields
        int id = 0;

        BGL_FORALL_VERTICES(v, g, DiGraph) {
            g[v].uid = ++id;
            uid2vertex.insert(std::make_pair(g[v].uid, v));
        }
    }

    int getRoot(const DiGraph& g) {
        std::vector<DiGraph::vertex_descriptor> ans;
        // search for all vertices of zero in-degree 

        BGL_FORALL_VERTICES(v, g, DiGraph) {
//            if (in_degree(v, g) == 0) {
//                ans.push_back(v);
//            }
        
            if (g[v].label == "main")
                ans.push_back(v);
        }

        assert(ans.size() == 1);
        return g[ans[0]].uid;
    }
}
