//
//  Graph.h
//  CallgraphVisualizer
//
//  Created by robin on 3/16/14.
//  Copyright (c) 2014 Lie Yan. All rights reserved.
//

#ifndef __CallgraphVisualizer__Graph__
#define __CallgraphVisualizer__Graph__


#ifdef __APPLE__
#include "TargetConditionals.h"
#ifdef TARGET_OS_MAC
#include <GLUT/glut.h>
#include <OpenGL/OpenGL.h>
#endif
#elif defined _WIN32 || defined _WIN64
#include <windows.h>
#include <GL/glut.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif


#include <iostream>
#include <string>
#include <unordered_map>
#include <unordered_set>

#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/topology.hpp>
#include <boost/random/linear_congruential.hpp>
#include <boost/progress.hpp>
#include <boost/graph/fruchterman_reingold.hpp>
#include <boost/graph/random_layout.hpp>

#include <boost/graph/graphviz.hpp>
#include <boost/graph/iteration_macros.hpp>

#include <ogdf/basic/Graph.h>
#include <ogdf/basic/GraphAttributes.h>
#include <ogdf/cluster/ClusterGraph.h>
#include <ogdf/basic/SList.h>
#include "vec2.h"
#include "font_manager.h"
#include "draw_utility.h"
#include "mouse.h"
#include "util.h"
#include "graph_type.h"
#include "GraphNode.h"

class MyGraph
{
private:
    DiGraph g;
    boost::dynamic_properties dp;
    std::unordered_map<int, DiGraph::vertex_descriptor> uid2vertex;
    std::unordered_map<int, ogdf::node> uid2ognode;
    //  std::unordered_set<int> selected;

    typedef ogdf::GraphAttributes GraphAttributes;
    ogdf::Graph OG;
    GraphAttributes GA;

    int level_to_expand;
    int rootUid;
    Node* graph;
    std::stack<int> history;

public:


    MyGraph(const std::string & path);

    virtual ~MyGraph();

    void getHierarchicalLayout(int width, int height);
    void setLevelToExpand(int level);
    void print();
    void Render();
    void Pick();
    void reconstruct();
    void constructMain();
    bool onPick(int uid, const MouseAction& mouseAction);
    void undo();
    /**
     * @param uid
     * @param mouseState
     * @return true if re-rendering is required
     */

    unsigned long getNumPickIds(); // return the number of pick-id's

protected:
    void constructGraph(int uid);
    void createCluster(int uid);
    int getRoot();
    double getWeight(int uid);
    int getOutDegree(int uid);
    std::string getLabel(int uid);
    void getForceDirectedLayout(int width, int height);
    std::vector<NodePtr> getOutNeighbors(int);
    std::vector<NodePtr> getInNeighbors(int uid);

    Vertex& getVertexByUid(int uid);


};


#endif /* defined(__CallgraphVisualizer__Graph__) */
