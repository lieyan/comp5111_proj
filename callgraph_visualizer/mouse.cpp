/* 
 * File:   MouseState.cpp
 * Author: robin
 * 
 * Created on March 28, 2014, 8:45 PM
 */

#include "mouse.h"

using namespace std::chrono;

MouseState::MouseState() {
    _x = _y = 0;
    leftBtnPressed = rightBtnPressed = false;
    timestamp = clock_now();
}

MouseState::MouseState(int x, int y, bool lbPressed, bool rbPressed) {
    this->_x = x;
    this->_y = y;
    this->leftBtnPressed = lbPressed;
    this->rightBtnPressed = rbPressed;
    this->timestamp = clock_now();
}

MouseState::MouseState(int x, int y, bool lbPressed, bool rbPressed,
        const timestamp_t& t) {
    this->_x = x;
    this->_y = y;
    this->leftBtnPressed = lbPressed;
    this->rightBtnPressed = rbPressed;
    this->timestamp = t;
}

MouseState::MouseState(const MouseState& orig) {
    this->_x = orig._x;
    this->_y = orig._y;
    this->leftBtnPressed = orig.leftBtnPressed;
    this->rightBtnPressed = orig.rightBtnPressed;
    this->timestamp = orig.timestamp;
}

MouseState::~MouseState() {
}

void MouseState::setPosition(int x, int y) {
    this->_x = x;
    this->_y = y;
}

int MouseState::x() const {
    return _x;
}

int MouseState::y() const {
    return _y;
}

void MouseState::setLeftDown(bool b) {
    this->leftBtnPressed = b;
}

void MouseState::setRightDown(bool b) {
    this->rightBtnPressed = b;
}

bool MouseState::isLeftDown() const {
    return this->leftBtnPressed;
}

bool MouseState::isRightDown() const {
    return this->rightBtnPressed;
}

void MouseState::setTimestamp(const timestamp_t& t) {
    this->timestamp = t;
}

timestamp_t MouseState::getTimestamp() const {
    return this->timestamp;
}




MouseAction::MouseAction() {
    this->_action = MouseActionType::MOVE;
}

int MouseAction::x() const {
    return transientPast.x();
}

int MouseAction::y() const {
    return transientPast.y();
}

int MouseAction::oldX() const {
    return btnChngEvt.x();
}

int MouseAction::oldY() const {
    return btnChngEvt.y();
}

MouseActionType MouseAction::action() const {
    return _action;
}


void MouseAction::update(const MouseState& state) {
    bool isMove = !(transientPast.x() == state.x()
            && transientPast.y() == state.y());
    bool isBtnEvt = (btnChngEvt.isLeftDown() != state.isLeftDown()
            || btnChngEvt.isRightDown() != state.isRightDown());
    transientPast = state;

    if (isBtnEvt) {
        if (btnChngEvt.isLeftDown() && !state.isLeftDown()
                && btnChngEvt.x() == state.x()
                && btnChngEvt.y() == state.y()) {
            _action = MouseActionType::LEFT_CLICK;
        } else
            if (btnChngEvt.isRightDown() && !state.isRightDown()
                && btnChngEvt.x() == state.x()
                && btnChngEvt.y() == state.y()) {
            _action = MouseActionType::RIGHT_CLICK;
        } else {
            _action = isMove ? MouseActionType::MOVE : MouseActionType::NONE;
        }

        btnChngEvt = state;
    } else {
        if (state.isLeftDown() && isMove) {
            _action = MouseActionType::DRAG;
        } else if (isMove){
            _action = MouseActionType::MOVE;
        }
    }
}


//double MouseAction::millisecondsToOldTimestamp(const MouseState& state) {
//    milliseconds_t time_span =
//            duration_cast<milliseconds_t> (state.getTimestamp() - old.getTimestamp());
//    return time_span.count();
//}



std::ostream& operator <<(std::ostream& o, MouseState const& state) {
    o << "(" << state.x() << ", " << state.y() << ")";

    //    std::time_t tt = to_time_t(state.getTimestamp());
    //    o << "timestamp: " << ctime(&tt);
    if (state.isLeftDown())
        o << "(left pressed)";
    if (state.isRightDown())
        o << "(right pressed)";
    return o;
}

std::ostream& operator<<(std::ostream& o, MouseAction const& act) {
    o << "mouse action is: ";
    switch (act.action()) {
        case MouseActionType::MOVE:
            o << "MOVE";
            break;
        case MouseActionType::LEFT_CLICK:
            o << "LEFT_CLICK";
            break;
        case MouseActionType::RIGHT_CLICK:
            o << "RIGHT_CLICK";
            break;
        case MouseActionType::DRAG:
            o << "DRAG";
            o << ";";
            o << act.btnChngEvt << "--> " << act.transientPast;
            
            break;
        case MouseActionType::NONE:
            o << "NONE";
            break;
    }

    return o;

}
