/* 
 * File:   TreeMapController.h
 * Author: robin
 *
 * Created on April 15, 2014, 4:02 PM
 */

#ifndef TREEMAPCONTROLLER_H
#define	TREEMAPCONTROLLER_H

#include "graph_type.h"
#include "Tree.h"
#include <string>
#include <memory>
#include <stack>

class TreeMapController
{
public:
    TreeMapController(const std::string& path);
    virtual ~TreeMapController();

    void Render();
    void Pick();
    bool onPick(int uid, const MouseAction& mouseAction);

    void undo();

    void setLevelToExpand(int level);

    void reconstruct();

protected:
    void constructTree(int uid);
    std::vector<TreeNodePtr> getOutNeighbors(int uid);
    int getOutDegree(int uid);
    double getWeight(int uid);
    std::string getLabel(int uid);

private:
    DiGraph g;
    boost::dynamic_properties dp;
    std::unordered_map<int, DiGraph::vertex_descriptor> uid2vertex;
    Tree* tree;

    int level_to_expand;
    std::stack<int> history;
    int rootUid;
};

#endif	/* TREEMAPCONTROLLER_H */

