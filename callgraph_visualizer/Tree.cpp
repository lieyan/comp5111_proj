/* 
 * File:   Tree.cpp
 * Author: robin
 * 
 * Created on April 13, 2014, 1:23 PM
 */

#include <memory>

#include "Tree.h"

using namespace draw_utility;

TreeNodePtr makeTreeNode(int uid, double weight, std::string label)
{
    assert(weight > 0);
    TreeNodePtr ans = new TreeNode(uid, weight);
    ans->setLabel(label);
    return ans;
}

TreeNode::TreeNode(int uid, double weight)
{
    assert(weight > 0);
    this->uid = uid;
    this->weight = weight;
    this->_visible = false;
}

Tree::~TreeNode()
{
    for (auto p : children) {
        delete p;
    }
    printf("TreeNode %d destroyed\n", this->uid);
}

int TreeNode::getUid() const
{
    return uid;
}

int TreeNode::getWeight() const
{
    return weight;
}

void TreeNode::setWeight(int weight)
{
    assert(weight > 0);
    this->weight = weight;
}

std::string TreeNode::getLabel() const
{
    return label;
}

void TreeNode::setLabel(std::string label)
{
    this->label = label;
}

void TreeNode::addChild(const TreeNodePtr& child)
{
    children.push_back(child);
}

void TreeNode::addChildren(const std::vector<TreeNodePtr>& rhs)
{
    for (const TreeNodePtr& t : rhs)
        children.push_back(t);
}

rect TreeNode::getBox() const
{
    return box;
}

void TreeNode::setBox(const rect& box)
{
    this->box = box;
}

/**
 * 
 * @return whether the node is fat 
 */
bool TreeNode::fat()
{
    int lineHeight = draw_utility::lineHeight();
    return (box.height() >= 2 * lineHeight && box.width() >= lineHeight);
}

bool TreeNode::node_visible()
{
    return _visible;
}

bool TreeNode::label_visible()
{
    rect label_box = draw_utility::BBox(this->label);
    return label_box.width() <= box.width() && label_box.height() <= box.height();
}

void TreeNode::recursive_nested_squalify(const rect& box)
{
    int margin_width = 4;

    this->_visible = true;

    if (children.empty())
        return;

    int lineHeight = draw_utility::lineHeight();

    if (!this->fat()) { // thin node will not be subdivided
        for (TreeNodePtr& p : children)
            p->_visible = false;
        return;
    }
    else {
        rect newBox = box.shrinkBy(margin_width,
                                   margin_width,
                                   lineHeight,
                                   margin_width); // reserve space for the label
        this->squarify(newBox);
        for (TreeNodePtr& p : children) {
            p->recursive_nested_squalify(p->getBox());
        }
    }
}

void TreeNode::squarify(const rect& box)
{
    if (children.empty())
        return;

    int sumWeight = 0;
    for (TreeNodePtr c : children) {
        sumWeight += c->getWeight();
    }

    std::vector<TreeNodePtr> row;
    int cur = 0;
    row.push_back(children[cur]);
    cur++;
    this->squarify(cur, row, box, sumWeight);
}

void TreeNode::squarify(int idxCurrNode, std::vector<TreeNodePtr>& row,
                        const rect& box, int sumWeightUnlaid)
{
    // the end of squarification
    if (idxCurrNode == this->children.size()) {
        layoutRowFill(row, box, sumWeightUnlaid);
        return;
    }

    // computer prettiness measure
    double worstOld, worstNew;
    auto worst = [](std::vector<TreeNodePtr>& row, const rect& box, int sumWeightUnlaid) ->double
    {
        assert(!row.empty());

        double shortSide = std::min(box.height(), box.width());
        double sumW = 0, minW = row[0]->weight, maxW = row[0]->weight;
        for (TreeNodePtr p : row) {
            sumW += p->weight;
            minW = std::min((double) p->getWeight(), minW);
            maxW = std::min((double) p->getWeight(), maxW);
        }
        double sumA = sumW / sumWeightUnlaid * box.area();
        double minA = minW / sumWeightUnlaid * box.area();
        double maxA = maxW / sumWeightUnlaid * box.area();
        return std::max(shortSide * shortSide * maxA / (sumA * sumA),
                        sumA * sumA / (shortSide * shortSide * minA));
    };

    worstOld = worst(row, box, sumWeightUnlaid);
    TreeNodePtr c = children[idxCurrNode];
    row.push_back(c);
    worstNew = worst(row, box, sumWeightUnlaid);
    row.pop_back();

    //    printf("worstOld = %lf, worstNew = %lf\n", worstOld, worstNew);
    // branch
    if (worstOld >= worstNew) {
        row.push_back(c);
        squarify(idxCurrNode + 1, row, box, sumWeightUnlaid);
    }
    else {
        rect newBox = layoutRow(row, box, sumWeightUnlaid);
        for (TreeNodePtr p : row) {
            sumWeightUnlaid -= p->getWeight();
        }
        std::vector<TreeNodePtr> newRow;
        newRow.push_back(c);
        squarify(idxCurrNode + 1, newRow, newBox, sumWeightUnlaid);
    }
}

void TreeNode::printLayout()
{
    using std::cout;
    using std::endl;

    cout << "parent: " << box << endl;
    cout << "children: " << endl;
    for (auto n : children) {
        cout << n->box << endl;
    }
}

rect TreeNode::layoutRow(const std::vector<TreeNodePtr>& row, 
                         const rect& box, int sumWeightUnlaid)
{
    double sumWeightInRow = 0.0;
    for (TreeNodePtr p : row) {
        sumWeightInRow += p->getWeight();
    }

    if (box.width() >= box.height()) {
        auto newBox = box.cutAtWidth(0.0, sumWeightInRow / sumWeightUnlaid);
        layoutRowFill(row, newBox, sumWeightInRow);
        return box.cutAtWidth(sumWeightInRow / sumWeightUnlaid, 1.0);
    }
    else {
        auto newBox = box.cutAtHeight(0.0, sumWeightInRow / sumWeightUnlaid);
        layoutRowFill(row, newBox, sumWeightInRow);
        return box.cutAtHeight(sumWeightInRow / sumWeightUnlaid, 1.0);
    }
}

void TreeNode::layoutRowFill(const std::vector<TreeNodePtr>& row,
                             const rect& box, int unlaidWeigthInBox)
{
    auto func = [&](double p1, double p2) ->rect
    {
        return (box.width() > box.height())
                ? box.cutAtWidth(p1, p2) : box.cutAtHeight(p1, p2);
    };

    //    printf("row size = %lu\n", row.size());

    int sumW = 0;
    for (TreeNodePtr p : row) {
        p->box = func(1.0 * sumW / unlaidWeigthInBox,
                      1.0 * (sumW + p->weight) / unlaidWeigthInBox);
        sumW += p->weight;
    }
}

void TreeNode::Render()
{
    if (!node_visible())
        return;


    drawRectangle(box.getTopLeft(), box.getBottomRight());

//    vec2 tl =box.getTopLeft();
//    vec2 br = box.getBottomRight();
//    printf("draw(box((%.2lf,%.2lf), (%.2lf,%.2lf)));\n", tl[0], 600-tl[1], br[0], 600-br[1]);

    if (label_visible()) {
        drawLabel(box.getTopLeft() + vec2(1, -6), this->label.c_str());

//        printf("label(\"$%s$\", (%.2lf, %.2lf), SE);\n", this->label.c_str(), tl[0], 600-tl[1]);
    }

    for (TreeNodePtr p : children) {
        p->Render();
    }
}

void TreeNode::Pick()
{
    if (!node_visible())
        return;

    GLubyte color[3];
    utility::id2color(uid, color);
    glColor3ubv(color);
    drawPickRectangle(box.getTopLeft(), box.getBottomRight());
    for (TreeNodePtr p : children) {
        p->Pick();
    }
}

