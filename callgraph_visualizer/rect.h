/* 
 * File:   rect.h
 * Author: robin
 *
 * Created on April 13, 2014, 1:48 PM
 */

#ifndef RECT_H
#define	RECT_H


#include "vec2.h"


#include <iostream>

class rect
{
public:
    rect();
    rect(double x1, double y1, double x2, double y2);
    rect(const rect& orig);
    virtual ~rect();

    double area() const;
    double height() const;
    double width() const;

    vec2 getTopLeft() const;
    vec2 getBottomRight() const;


    /**
     * Suppose the box is [x1,x2]x[y1, y2].
     * 
     * @param portion1
     * @param portion2
     * @return [x1+portion1*(x2-x1), x1+portion2*(x2-x1)]x[y1, y2]
     */
    rect cutAtWidth(double portion1, double portion2) const;
    /**
     * Suppose the box is [x1,x2]x[y1, y2].
     * @param portion1
     * @param portion2
     * @return [x1,x2]x[y1+portion1*(y2-y1), y1+portion2*(y2-y1)]
     */
    rect cutAtHeight(double portion1, double portion2) const;

    /**
     * Suppose the box is [x1,x2]x[y1,y2].
     * @param left
     * @param right
     * @param top
     * @param bottom
     * @return [x1-left, x2-right]x[y1-top,y2-bottom]
     */
    rect shrinkBy(double left, double right, double top, double bottom) const;

    friend std::ostream& operator<<(std::ostream& o, rect const& r);

private:
    vec2 topLeft; // left top
    vec2 bottomRight; // right bottom
};

std::ostream& operator<<(std::ostream& o, rect const& r);


#endif	/* RECT_H */

