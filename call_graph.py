try:
    graph = ximport("graph")
except ImportError:
    graph = ximport("__init__")
    reload(graph)
    
import en
import networkx as nx
import pygraphviz
import os

def remove_direction(g):
    h = nx.Graph()
    h.add_nodes_from(g.nodes(data=True))
    
    for e in g.edges():
        h.add_edge(e[0],e[1])
        h.add_edge(e[1],e[0])
    return h

def construct_graph(g):
    h = graph.create(iterations=10, distance=1.3, 
                           layout="circle", depth=True)
    for v in g.nodes():
        n = h.add_node(v)    
        if g.node[v].has_key('label'):
            n.label = g.node[v]['label']
    for e in g.edges():
        h.add_edge(e[0], e[1])
    return h

work_dir = "/Users/robin/2014spring/software_analysis/project"
os.chdir(work_dir)
G = nx.DiGraph(nx.read_dot("sample1.dot"))
H = remove_direction(G)

small_graph = construct_graph(G)
small_graph.events.popup = True

size(400, 500)
speed(30)

def draw():
    small_graph.styles.textwidth = 120
    small_graph.draw(
        directed=True, 
        weighted=True,
        traffic=True
    )
    
