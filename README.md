#Introdution
This project was an attempt to visualize the callgraph of program. It was originally a course project for COMP5111 (Software Analysis) at HKUST. The idea of the project was proposed by Lie Yan and Jingjie Jiang. 


#Screenshot
Take a look at `screenshot` folder. One picture is worth a thousand words. 


#Build
Steps to visualize a callgraph

0. Generate a .bc file of the project to visualize. For details see REAME.md of https://github.com/qingkaishi/whole-program-llvm. 
	oout << sizeof(int) << endl;
1. Analyze the .bc file using opt/canay to get a .dot file. For usage of the two commands, see analyze.sh.
2. Apply to_simple.py to the .dot file to convert a multi-digraph to a simple digraph. 
3. Run our program


To compile our program, you need deploy:

1. Boost Graph Library
2. OGDF, Open Graph Drawing Framework
3. Osi, a linear solver
4. FTGL, a font rendering library for Open GL